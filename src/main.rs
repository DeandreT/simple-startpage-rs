#[macro_use]
extern crate rocket;

use rocket::fs::NamedFile;
use rocket::response::status::NotFound;
use rocket_dyn_templates::{context, Template};
use serde::{Deserialize, Serialize};
use std::path::{Path, PathBuf};
use std::process::Command;
use std::fs::File;

#[derive(Serialize, Deserialize, Debug)]
struct Link {
    url: String,
    fa_icon: String,
}

fn get_uptime() -> String {
    let uptime = String::from_utf8(
        Command::new("uptime")
            .arg("-p")
            .output()
            .expect("Failed to execute command.")
            .stdout
            .strip_prefix("up ".as_bytes())
            .expect("Invalid prefix").to_vec())
    .unwrap_or("Uptime unavailable".to_string());
    uptime
}

#[get("/")]
async fn index() -> Template {
    let links: Vec<Link> = serde_json::from_reader(File::open("content/links.json").expect("File couldn't be opened")).unwrap();
    let uptime = get_uptime();
    Template::render("index", context! { uptime, links })
}

#[get("/static/<file..>")]
async fn static_files(file: PathBuf) -> Result<NamedFile, NotFound<String>> {
    let path = Path::new("static/").join(file);
    NamedFile::open(&path)
        .await
        .map_err(|e| NotFound(e.to_string()))
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .mount("/", routes![index, static_files])
        .attach(Template::fairing())
}
